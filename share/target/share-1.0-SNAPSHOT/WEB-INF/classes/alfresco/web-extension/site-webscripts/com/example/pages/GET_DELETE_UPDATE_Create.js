model.jsonModel = {
		services: [
		           "alfresco/services/CrudService"
		           ]
};

var list = {
		name: "alfresco/lists/AlfList",
		config: {
			loadDataPublishTopic: "ALF_CRUD_GET_ALL",
			loadDataPublishPayload: {
				url: "slingshot/datalists/lists/site/test/dataLists"
			},
			itemsProperty: "datalists"
		}
};

var views = [
             {
            	 name: "alfresco/documentlibrary/views/AlfDocumentListView",
            	 config: {
            		 widgets: [
            		           {
            		        	   id: "VIEW_ROW",
            		        	   name: "alfresco/documentlibrary/views/layouts/Row",
            		        	   config: {
            		        		   widgets: [
            		        		             {
            		        		            	 name: "alfresco/documentlibrary/views/layouts/Cell",
            		        		            	 config: {
            		        		            		 widgets: [
            		        		            		           {
            		        		            		        	   id: "DATA_LIST_TITLE",
            		        		            		        	   name: "alfresco/renderers/Property",
            		        		            		        	   config: {
            		        		            		        		   propertyToRender: "title"
            		        		            		        	   }
            		        		            		           }
            		        		            		           ]
            		        		            	 }
            		        		             }
            		        		             ]
            		        	   }
            		           }
            		           ]
            	 }
             }];

list.config.widgets = views;
model.jsonModel.widgets = [list];

var deleteCell = {
		name : "alfresco/documentlibrary/views/layouts/Cell",
		config : {
			widgets : [ {
				name : "alfresco/renderers/PublishAction",
				config : {
					iconClass : "delete-16",
					propertyToRender : "title",
					altText : "Delete {0}",
					publishTopic : "ALF_CRUD_DELETE",
					publishPayloadType : "PROCESS",
					publishPayload : {
						requiresConfirmation : true,
						url : "slingshot/datalists/list/node/{nodeRef}",
						confirmationTitle : "Delete Data List",
						confirmationPrompt : "Are you sure you want to delete '{title}'?",
						successMessage : "Successfully deleted '{title}'"
					},
					publishPayloadModifiers : [ "processCurrentItemTokens",
					                            "convertNodeRefToUrl" ]
				}
			} ]
		}
};

var viewRow = widgetUtils.findObject(model.jsonModel.widgets, "id", "VIEW_ROW");
viewRow.config.widgets.push(deleteCell);  
var dataListTitle = widgetUtils.findObject(model.jsonModel.widgets, "id", "DATA_LIST_TITLE");
dataListTitle.name = "alfresco/renderers/InlineEditProperty"; //widget che da la possibilità di fare editing delle proprietà "in Line"
dataListTitle.config = {
		propertyToRender: "title",
		postParam: "prop_cm_title",//è settato quando propertyToRender non fa match con i parametri di richiesta del REST API
		refreshCurrentItem: true, //è settato per assicurare che qualunque cambiamento verrà applicato a tutti gli altri widget
		//che dipendono da questo oggetto
		requirementConfig: { // è lo stesso usato nei FORM Aikau widgets e questo pezzo di configurazione è delegato al FORM Control
			// in questo caso a alfresco/forms/controls/DojoValidationTextBox  (widget)
			initialValue: true
		},
		publishTopic: "ALF_CRUD_CREATE",
		publishPayloadType: "PROCESS",
		publishPayloadModifiers: ["processCurrentItemTokens", "convertNodeRefToUrl"],
		publishPayloadItemMixin: false, // per prevenire behaviour di default incluso l'oggetto currentItem
		publishPayload: {
			url: "api/node/{nodeRef}/formprocessor",
			noRefresh: true, //true: per sovrascrivere il default behaviour del publishing per refresh della lista
			successMessage: "Update success"
		}
};
var alfDestination = null;
var result = 
	remote.call("/slingshot/datalists/lists/site/test/dataLists");
if (result.status.code == status.STATUS_OK)
{
	alfDestination = JSON.parse(result).container;
}       
var formControls = [
                    {
                    	name: "alfresco/forms/controls/DojoValidationTextBox",
                    	config: {
                    		name: "alf_destination",
                    		value: alfDestination,
                    		visibilityConfig: {
                    			initialValue: false
                    		}
                    	}
                    },
                    {
                    	name: "alfresco/forms/controls/DojoValidationTextBox",
                    	config: {
                    		label: "Title",
                    		name: "prop_cm_title",
                    		requirementConfig: {
                    			initialValue: true
                    		}
                    	}
                    },
                    {
                    	name: "alfresco/forms/controls/DojoTextarea",
                    	config: {
                    		label: "Description",
                    		name: "prop_cm_description"
                    	}
                    },
                    {
                    	name: "alfresco/forms/controls/DojoSelect",
                    	config: {
                    		label: "List Type",
                    		name: "prop_dl_dataListItemType",
                    		value: "dl:event",
                    		optionsConfig: {
                    			publishTopic: "ALF_GET_FORM_CONTROL_OPTIONS",
                    			publishPayload: {
                    				url: url.context + "/proxy/alfresco/api/classes/dl_dataListItem/subclasses", 
                    				itemsAttribute: "",
                    				labelAttribute: "title",
                    				valueAttribute: "name"
                    			}
                    		}
                    	}
                    }
                    ];         
var button = {
		  name: "alfresco/buttons/AlfButton",
		  config: {
		    label: "New List",
		    additionalCssClasses: "call-to-action",
		    publishTopic: "ALF_CREATE_FORM_DIALOG_REQUEST",
		    publishPayloadType: "PROCESS",
		    publishPayloadModifiers: ["processCurrentItemTokens"],
		    publishPayload: {
		      dialogTitle: "New List",
		      dialogConfirmationButtonTitle: "Save",
		      dialogCancellationButtonTitle: "Cancel",
		      formSubmissionTopic: "ALF_CRUD_CREATE",
		      formSubmissionPayloadMixin: {
		        url: "api/type/dl%3AdataList/formprocessor"
		      },
		      fixedWidth: true,
		      widgets: formControls
		    }
		  }
		};         
model.jsonModel.services.push("alfresco/dialogs/AlfDialogService",
"alfresco/services/OptionsService");
model.jsonModel.widgets.splice(0, 0, button);                        
