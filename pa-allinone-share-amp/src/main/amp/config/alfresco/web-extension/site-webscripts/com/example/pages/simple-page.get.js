var toggleTopic = "TOPIC";

var showSurfLogoRules = {
  initialValue: false,
  rules: [
    {
      topic: toggleTopic,
      attribute: "show",
      is: [true],
      isNot: [false]
    }
  ]
};

var showAlfrescoLogoRules = {
  initialValue: true,
  rules: [
    {
      topic: toggleTopic,
      attribute: "show",
      is: [false],
      isNot: [true]
    }
  ]
};

var showAlfrescoLogo = {
  name: "alfresco/renderers/PropertyLink",
  config: {
    visibilityConfig: showAlfrescoLogoRules,
    currentItem: {
      label: "Show Alfresco Logo"
    },
    propertyToRender: "label",
    useCurrentItemAsPayload: false,
    publishTopic: toggleTopic,
    publishPayloadType: "CONFIGURED",
    publishPayload: {
      show: true
    }
  }
};
var showSurfLogo = {
  name: "alfresco/renderers/PropertyLink",
  config: {
    visibilityConfig: showSurfLogoRules,
    currentItem: {
      label: "Show Surf Logo"
    },
    propertyToRender: "label",
    useCurrentItemAsPayload: false,
    publishTopic: toggleTopic,
    publishPayloadType: "CONFIGURED",
    publishPayload: {
      show: false
    }
  }
};

var alfrescoLogo = {
  name: "alfresco/logo/Logo",
  config: {
    logoClasses: "alfresco-logo-large",
    visibilityConfig: showSurfLogoRules
  }
};
var surfLogo = {
  name: "alfresco/logo/Logo",
  config: {
    logoClasses: "surf-logo-large",
    visibilityConfig: showAlfrescoLogoRules
  }
};

model.jsonModel = {
  widgets: [
    {
      name: "alfresco/layout/VerticalWidgets",
      config: {
        widgets: [
          showAlfrescoLogo,
          showSurfLogo,
          alfrescoLogo,
          surfLogo
        ]
      }
    }
  ]
};
                    