var fixedWidth = {
	name : "alfresco/layout/ClassicWindow",
	widthPx : "300",
	config : {
		title : "Always 300px"
	}
};

var dynamicWidth = {
	name : "alfresco/layout/ClassicWindow",
	widthPc : "50",
	config : {
		title : "50% after fixed deductions"
	}
};

var auto1 = {
	name : "alfresco/layout/ClassicWindow",
	config : {
		title : "Share remainder"
	}
};
var auto2 = {
	name : "alfresco/layout/ClassicWindow",
	config : {
		title : "Share remainder"
	}
};

model.jsonModel = {
	widgets : [ {
		name : "alfresco/layout/HorizontalWidgets",
		config : {
			widgetMarginLeft : "5",
			widgetMarginRight : "5",
			widgets : [ fixedWidth, dynamicWidth, auto1, auto2 ]
		}
	} ]
};                        
                        